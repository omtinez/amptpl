const gulp = require('gulp');
const pckg = require('./package.json');

// Process arguments
const args = require('minimist')(process.argv.slice(1));
const env = args.env || 'development';


// Lint tasks

gulp.task('lint:html', function() {
    const htmlhint = require('gulp-htmlhint');
    return gulp.src(['src/**/*.html', '!src/**/*.tpl.html', '!src/**/*.frame.html'])
        .pipe(htmlhint())
        .pipe(htmlhint.reporter('fail'));
});

gulp.task('lint:js', function() {
    const jshint = require('gulp-jshint');
    return gulp.src(['src/**/*.js'])
        .pipe(jshint({ newcap: false, sub: true }))
        .pipe(jshint.reporter('fail'));
});

gulp.task('lint:all', gulp.series('lint:html', 'lint:js'));


// Clean tasks

gulp.task('clean', function() {
    const del = require('del');
    return del(['public/*', 'dist']);
});


// Build tasks

gulp.task('build:css', function() {
    const merge = require('merge2');
    const concat = require('gulp-concat');
    const cssmin = require('gulp-clean-css');
    const rename = require('gulp-rename');
    const replace = require('gulp-replace');
    const mancha = require('gulp-mancha');

    const stream = gulp
        .src(['src/**/*.css'])
        .pipe(mancha({
            'theme-primary': pckg.amptpl.theme.primary,
            'theme-accent': pckg.amptpl.theme.accent,
            'theme-background': pckg.amptpl.theme.background
        }));

    return merge(gulp.src(['node_modules/muicss/dist/css/mui.min.css']), stream)
        .pipe(replace('!important', ''))
        .pipe(concat('styles.css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/static'))
});

gulp.task('build:amp', function() {
    const mancha = require('gulp-mancha');

    // Dynamically identify variables from package.json
    const vars = pckg.amptpl;
    vars.name = pckg.displayName;
    vars.description = pckg.description;

    // Add variables from context
    vars.env = env;
    vars.year = new Date().getFullYear();

    // Perform rendering
    return gulp.src(['src/**/*.html', '!src/**/*.tpl.html'])
        .pipe(mancha(vars, {
            console: console,
            canonical: null
        }))
        .pipe(gulp.dest('public'));
});

gulp.task('build:logo', function() {
    const jimp = require('gulp-jimp');
    const imagesizes = [64, 128, 512];
    return gulp.src('src/static/logo.png')
    .pipe(jimp(
        imagesizes.reduce(function(acc, size) {
            acc['-' + size] = {resize: {width: size}};
            return acc;
        }, {})
    ))
    .pipe(gulp.dest('public/static'));
});

gulp.task('build:ts', function() {
    const ts = require('gulp-typescript');
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'commonjs',
            declaration: true,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('dist'));
});

gulp.task('build:all', gulp.series('build:css', 'build:logo', 'build:amp', 'build:ts'));


// Copy tasks

gulp.task('copy:static', function() {
    return gulp.src(['src/**/*', '!src/**/*.ts', '!src/**/*.css', '!src/**/*.html'])
        .pipe(gulp.dest('public'));
});

gulp.task('copy:catalog', function() {
    return gulp.src(['catalog/**/*'])
        .pipe(gulp.dest('public/static'));
});

gulp.task('copy:all', gulp.series('copy:static', 'copy:catalog'));


// Minification tasks

gulp.task('minify:js', function() {
    const uglify = require('gulp-uglify');
    return gulp.src(['public/**/*.js', '!public/node_modules/**/*'])
        .pipe(uglify())
        .pipe(gulp.dest('public'));
});

gulp.task('minify:html', function() {
    const minify = require('gulp-minify-inline');
    return gulp.src(['public/**/*.html', '!public/node_modules/**/*'])
        .pipe(minify({jsSelector: 'script[data-do-not-minify!=true]'}))
        .pipe(gulp.dest('public'));
});

gulp.task('minify:img', function() {
    const imagemin = require('gulp-imagemin');
    return gulp.src(['src/**/*.png', 'src/**/*.jpg', 'src/**/*.jpeg', 'src/**/*.gif'])
        .pipe(imagemin())
        .pipe(gulp.dest('public'));
});

gulp.task('minify:all', gulp.series('minify:js', 'minify:html', 'minify:img'));


// Validation tasks

gulp.task('amp:validate', function() {
    const amp = require('gulp-amphtml-validator');
    return gulp.src(['public/**/*.html', '!public/**/*.frame.html', '!public/node_modules/**/*.html'])
        .pipe(amp.validate())
        .pipe(amp.format())
        .pipe(amp.failAfterError());
});


// Deploy tasks

gulp.task('firebase', function(callback) {
    const client = require('firebase-tools');
    return client.deploy({
        project: pckg.name,
        token: process.env.FIREBASE_TOKEN
    }).then(function() {
        return callback();
    }).catch(function(err) {
        console.log(err);
        return process.exit(1);
    });
});

// Used to make sure the process ends
gulp.task('exit', function(callback) {
    callback();
    process.exit(0);
});


// High level tasks

gulp.task('build:debug', gulp.series('lint:all', 'build:all', 'copy:all'));
gulp.task('build:prod', gulp.series('build:debug', 'minify:all', 'amp:validate'));
gulp.task('build', gulp.series('build:prod'));
gulp.task('default', gulp.series('build'));
gulp.task('deploy', gulp.series('firebase', 'exit'));
