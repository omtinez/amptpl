# Web App Template
A simple template for web apps built using [MUI CSS](https://github.com/muicss/mui) and
[gulp-mancha](https://gitlab.com/omtinez/gulp-mancha). It will let you build out fast, responsive
AMP-based websites that are highly customizable.

## Usage
Clone this project and replace all references of `__project_name__` and `__project_description__`.
Don't forget to update this README too!
